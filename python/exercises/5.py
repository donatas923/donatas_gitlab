numbers = []
numbers2 = [num2 for num2 in range(1, 21, 2)]
numbers1 = [num1 for num1 in range(1,44)]
numbers3 = [num3 for num3 in range(3,31,3)]
numbers4 = [num4**3 for num4 in range(1,11)]
for num in range(1, 1000001):
	numbers.append(num)
print(numbers[34])
print(numbers1[40])

print("Длинна массива numbers равна : " + str(len(numbers)))
print("Длинна массива numbers1 равна : " + str(len(numbers1)))
print("Минимальное значение массива numbers : " + str(min(numbers)))
print("Минимальное значение массива numbers1 : " + str(min(numbers1)))
print("Максимальное значение массива numbers : " + str(max(numbers)))
print("Максимальное значение массива numbers1 : " + str(max(numbers1)))
print("Сумма значений массива numbers равна : " + str(sum(numbers)))
print("Сумма значений массива numbers1 равна : " + str(sum(numbers1)))
print('\n')
print("Список numbers2 : ")
for num2 in numbers2:
	print(num2)
print("Список numbers3 : ")
for num3 in numbers3:
	print(num3)
print("Список numbers4 : ")
for num4 in numbers4:
	print(num4)

print("The first three items in the list are : ")
srez = numbers[:3]
print(srez)
print("Three items from the middle of the list are : ")
srez1 = numbers1[20:23]
print(srez1)
print("The last three items in the list are : ")
srez2 = numbers1[-3:]
print(srez2)