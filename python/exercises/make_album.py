def make_album(artist_name, album_name, quantity_track = ''):
    if quantity_track:
        albums = {
                'Artist Name' : artist_name,
                'Album Name' : album_name,
                'Quantity Track' : quantity_track
                }
    else:
        albums = {
                'Artist Name'  : artist_name ,
                'Album Name' : album_name,
                }
    for key , value,  in albums.items():
        print(key + ":" + value)
    print('\n') 
    
artist_name = ''
album_name = ''

while (artist_name != 'exit' or album_name != 'exit'):
    artist_name = input('Input name of artist : ')
    if (artist_name == 'exit'):
        print('By')
        break;
    album_name = input('Input name of album : ')
    if (album_name == 'exit'):
        print('By')
        break
    make_album(artist_name, album_name)
    

