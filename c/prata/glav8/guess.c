#include <stdio.h>

int main(void)
{
	int guess = 1;
	printf("Введите целое число в интервале от 1 до 100. Я попробую его угадать\n");
	printf("Нажмите клавишу 'y', если моя догадка верна\n");
	printf("нажмите клавишу 'n', если моя догадка не верна\n");
	printf("Вашим число является %d ?\n", guess);
	while (getchar() != 'y')
	{
		printf("Ладно, тогда это %d?\n", ++guess);
		while (getchar() != '\n')
		{
			continue;
		}	
	}
	printf("Я знал, что у меня получится \n");
	
	return 0;
}


