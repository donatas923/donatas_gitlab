/* переполнение */

#include <stdio.h>

int main(void)
{
	unsigned int in;
	float flo;
	double dubl;
	
	printf("Размер целочисленного типа int = %zd байт \n", sizeof(unsigned int));
	printf("Размер типа с плавающей точной fload = %zd байт \n", sizeof(float));
	printf("Размер типа с плавающей точной double = %zd байт \n", sizeof(double));
	
	printf("Введите in = ");
	scanf("%u", &in);
	printf("in = %u \n", in);
	printf("Введите flo = ");
	scanf("%f", &flo);
	printf("flo = %f \n", flo);
	
	return 0;


}

	
