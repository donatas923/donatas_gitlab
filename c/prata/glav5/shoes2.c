#include <stdio.h>
#define ADJUST 7.31

int main(void)
{
	const double SCALE = 0.333;
	double shoe, foot;
	
	printf("shoes size (male)     length foot\n");
	shoe = 3.0;
	while (shoe < 20.5)
	{
		foot = SCALE * shoe + ADJUST;
		printf("%10.1f %20.2f inch\n", shoe, foot);
		shoe = shoe + 1.0;
	}
	printf("If the shoes fits, wear it\n");
	return 0 ;
}

