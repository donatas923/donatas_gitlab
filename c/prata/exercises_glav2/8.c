#include <stdio.h>

void on_three(void);
void two(void);

int main(void)
{
	printf("Начинаем: \n");
	on_three();
	printf("порядок!\n");

	return 0;
}

void on_three(void)
{
	printf("один \n");
	two();
	printf("три \n");
}

void two(void)
{
	printf("два \n");
}

