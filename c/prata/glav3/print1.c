/* print1.c --- демострирует некоторые свойста функции printf() */

#include <stdio.h>

int main(void)
{
	int ten = 10;
	int two = 2;

	printf("Выполняется правильно : \n");
	printf("%d минус %d равно %d \n", ten,2,ten - two);
	printf("Выполняется не верно : \n");
	printf("%d минус %d равно %d \n", ten);

	return 0;
}
