#include <stdio.h>
#define ROWS 4
#define COLS 5

void ini_mass(double (*ar)[COLS], int rows);
void show_mass(const double (*ar)[COLS], int rows);
void copy_mass(double (*ar)[COLS], int rows, double (*ar1)[COLS], int rows1);

int main(void){
	double mass0[ROWS][COLS];
	double mass1[ROWS][COLS];
	double mass2[ROWS][COLS];
	double mass3[ROWS][COLS];
	
	putchar('\n');
	ini_mass(mass0, ROWS);
	printf("mass0 : \n");
	show_mass(mass0, ROWS);
	copy_mass(mass0, ROWS, mass1, ROWS);
	putchar('\n');
	printf("mass1 : \n");
	show_mass(mass1, ROWS);
	copy_mass(mass0, ROWS, mass2, ROWS);
	putchar('\n');
	printf("mass2 : \n");
	show_mass(mass2, ROWS);
	copy_mass(mass0, ROWS, mass3, ROWS);
	putchar('\n');
	printf("mass3 : \n");
	show_mass(mass3, ROWS);
	

	return 0;
}
void ini_mass(double (*ar)[COLS], int rows){
	int r;
	int c;
	int a;

	for (r = 0; r < rows; r++){
		for (c = 0, a = 0; c < COLS; c++, a++){
			ar[r][c] = a + 1; 
		}
	}
}
void show_mass(const double (*ar)[COLS], int rows){
	int r;
	int c;
	int n;

	for (r = 0; r < rows; r++){
		for (c = 0; c < COLS; c++){
			printf("%f ", ar[r][c]);
		}
		putchar('\n');
	}
}
void copy_mass(double (*ar)[COLS], int rows, double (*ar1)[COLS], int rows1){
	int r;
	int c;

	for (r = 0; r < rows; r++){
		for (c = 0; c < COLS; c++){
			ar1[r][c] = ar[r][c];
		}
	}
}
