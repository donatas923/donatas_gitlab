# include <gtk/gtk.h>
 
int main( int argc, char *argv[]){
    GtkWidget *label;    // Метка
    GtkWidget *window;    // Главное окно
    /* Инициализация GTK+ */
    gtk_init(&argc, &argv);
 
    /* Создание главного окна приложения */
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
 
    /* Устанавливаем заголовок окна "Hello world!" */
    gtk_window_set_title(GTK_WINDOW(window), "Hello world!");
    /* Создаем текст "Hello world!"  в главном окне*/
    label = gtk_label_new("Hello world!");
 
    /* Вставляем его в главное окно */
    gtk_container_add(GTK_CONTAINER(window), label);
 
    /* Показываем окно вместе с виджетами */
    gtk_widget_show_all(window);
 
    /* Соединяем сигнал завершения с выходом из программы */
    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);
 
    /* Переход в бесконечный цикл ожидания действий пользователя */
    gtk_main();
    return 0;
}

