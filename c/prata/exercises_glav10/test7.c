#include <stdio.h>

int main(void)
{
	float rootbeer[10];
	float things[10][5];
	float *pf;
	float value = 2.2;

	printf("%p\n", &rootbeer);
	printf("%f\n", rootbeer);
	things[4][4] = rootbeer[3];
	pf = &value;
	printf("%f\n", *pf);
	pf = rootbeer;
	printf("%f\n", *pf);

	return 0;
}
