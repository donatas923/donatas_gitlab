#include <stdio.h>
#define MONTHS 12
#define YEARS 5

void year_rain(const float (*ar)[MONTHS], int years);
void months_rain(const float (*ar)[MONTHS], int years);

int main(void){
	const float rain[YEARS][MONTHS] = {
		{4.3,4.3,3.2,2.2,5.2,1.1,8.4,9.1,5.9,1.1,6.4,1.3},
		{5.2,1.5,3.3,9.1,3.2,1.4,8.4,7.3,7.2,2.1,8.3,9.1},
		{5.5,8.4,9.2,5.3,9.2,2.5,9.3,1.4,4.3,3.7,5.3,3.4},
		{8.2,2.1,4.9,2.5,6.3,2.1,5.6,7.2,6.7,9.2,5.6,6.3},
		{2.6,7.5,2.2,7.4,2.2,1.5,3.6,9.3,2.4,6.3,7.2,9.5}
	};

	year_rain(rain, YEARS);
	months_rain(rain, YEARS);

	return 0;
}
void year_rain(const float (*ar)[MONTHS], int years){
	
	int y;
	int m;
	float sum_years;
	
	printf("ГОД	КОЛЛИЧЕСТВАО ОСАДКОВ(в дюймах)\n");
	for (y = 0; y < years; y++){
		sum_years = 0;
		for (m = 0; m < MONTHS; m++){
			sum_years += ar[y][m];
		}	
		printf("%d %15.1f\n", 2010 + y, sum_years);
	}
}
void months_rain(const float (*ar)[MONTHS], int years){
	int y;
	int m;
	float mon_sum;
	
	printf("СРЕДНЕМЕСЯЧНОЕ КОЛИЧЕСВТВО ОСАДКОВ : \n\n");
	printf("янв  фев  мар  арп  май  Июн  Июл  Авг  Сен  Окт  Ноя  Дек\n");
	for (m = 0; m < MONTHS; m++){
		mon_sum = 0;
		for (y = 0; y < years; y++){
			mon_sum += ar[y][m];
		}
		printf("%4.1f", mon_sum / YEARS);
	}
	putchar('\n');
}
