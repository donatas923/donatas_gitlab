#include <stdio.h>
#include <string.h>

int main(void)
{
	char name[30];
	char surname[30];
	int length_name;
	int length_surname;
	
	printf("Input name : ");
	scanf("%s", name);
	length_name = strlen(name);

	printf("Input surname : ");
	scanf("%s", surname);
	length_surname = strlen(surname);

	printf(" name \t");
	printf("\t\tsurname\n");
	printf("-------------------------------------------------- \n");
	printf("%-15s", name);
	printf("\t\t%-15s\n", surname);
	printf("%-15d", length_name);
	printf("\t\t%-15d\n", length_surname);

	return 0;
}

