#include <stdio.h>
#include <float.h>

int main(void)
{
	float a = 1.0 / 3.0;
	double d = 1.0 / 3.0;
	
	printf("%.4f\n", a);
	printf("%.4f\n", d);
	
	printf("%.12f\n", a);
	printf("%.12f\n", d);
	
	printf("%.16f\n", a);
	printf("%.16f\n", d);
	
	printf("%f\n", FLT_DIG);
	printf("%f\n", DBL_DIG);
	
	return 0;
}


