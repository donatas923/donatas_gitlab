#include <stdio.h>

int main(void)
{
	float speed, size_file;
	printf("Input speed (MB/s) : ");
	scanf("%f", &speed);
	printf("Input size file (MB) : ");
	scanf("%f", &size_file);
	printf("Speed load : %.2f Mb/s\n", speed);
	printf("Size file : %.2f Mb\n", size_file);
	printf("Speed load %.2f Mb/s file size %.2f Mb download %.2f sec \n", speed, size_file, (size_file*1024*1024*8)/(speed*1000*1000));

	return 0;
}

