#include <stdio.h>

int main(void)
{
	float a;
	char n[30];
	
	printf("Input you name : ");
	scanf("%s", n);
	printf("Input growth : ");
	scanf("%f", &a);
	printf("%s, you growth %.2f centimetr \n", n, a);
	printf("%s, you growth %.2f inch \n", n, a * 0.393701);
	printf("%s, you growth %.2f foot \n", n , a * 0.0328084);

	return 0;
}

