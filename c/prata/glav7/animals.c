#include <stdio.h
#include <ctype.h>
#include <wchar.h>

int main(void)
{
	char ch;
	
	printf("Дайте мне букву алфавита, и я укажу вам называние животного \n начинающегося с этой буквы \n ");
	printf("Введите букву или # для завершения : ");
	while ((ch = getchar()) != '#')
	{
		if ('\n' == ch)
		{
			continue;
		}
		if (islower(ch))
		{
			switch (ch)
			{
			case 'a':
				printf("архар, дикий горный азиатский баран\n");
				break;
			case 'b':
				printf("бабирусса, дикая малайская свинья\n");
				break;
			case 'c':
				printf("коати, косуха обыкновенная \n");
				break;
			case 'd':
				printf("выхухоль, водоплавающее существо \n");
				break;
			case 'f':
				printf("ехидна , игольчатый муравьед \n");
				break;
			case 'g':
				printf("рыболов, светло-коричневая куница\n");
				break;	
			default :
				printf("Вопрос озадачил! \n");
			}
		}
		else 
		{
			printf("расспознаются только строчные буквы\n");
		}
		while (getchar() != '\n')
		{
			continue;
		}
		printf("Введите следующую букву или # для завершения \n");
	}
	printf("До свидания\n ");

	return 0;
}

