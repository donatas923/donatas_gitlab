#include <stdio.h>

int main(void)
{
	int num;
	
	printf("Введите num = ");
	scanf("%d", &num);
	
	if (!(num >= 1 && num <= 9))
	{
		printf("Верно");
	}
	else
	{
		printf("Не верно");
	}

	return 0;
}
