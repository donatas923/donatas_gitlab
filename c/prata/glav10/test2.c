#include <stdio.h>

void summ(int *x, int *y);

int main(void)
{
	int a = 10;
	int b = 10;
	
	summ(&a, &b);
	
	return 0;
}
void summ(int *x, int *y)
{
	int c;
	
	c = *x + *y;
	
	printf("x = %d\ny = %d\n", *x, *y);
	printf("c = %d\n", c);
}

