#include <stdio.h>
#define ROWS 3
#define COLS 4

void show(int ar[][COLS], int rows);

int main(void)
{
	int nums[3][4] = {
		{5,2,6,1},
		{3,5,2,9},
		{5,2,0,4},	
	};
	show(nums, ROWS);
}
void show(int ar[][COLS], int rows)
{
	int a;
	int b;

	for (a = 0; a < ROWS; a++)
	{
		for (b = 0; b < COLS; b++)
		{
			printf("%d", ar[a][b]);
		}
		putchar('\n');
	}
}

