#include <stdio.h>
#define SIZE 10

void filing(int *mass, int s);
void output_mass(const int *mass, int s);
void print_line(int l);

int main(void)
{
	int nums[SIZE];
	const int * const p = nums;
	int a; // size line
	
	printf("Желаемый размер разделитедя : ");
	while ((scanf("%d", &a)) == 1)
	{
		if (a > 60)
		{
			printf("Слишком много");
			continue;
		}
		print_line(a);
		filing(nums, SIZE);
		output_mass(nums, SIZE);
		print_line(a);
		continue;
	}
	
	printf("%d\n", *p);
	p = &nums[3];

	return 0;
}
void filing(int *mass, int s)
{
	int i;
	int x;
	
	for (i = 0, x = 0; i < s, x < s; i++, x++)
	{
		mass[i] = x * 2;
	}
}
void output_mass(const int *mass, int s)
{
	int i;

	for (i = 0; i < s; i++)
	{
		printf("%d элемент массива : %d\n", i, mass[i]);
	}
}
void print_line(int l)
{
	int i;

	for (i = 0; i <= l; i++)
	{
		printf("-");
	}
	putchar('\n');
}
