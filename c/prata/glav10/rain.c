#include <stdio.h>
#define MONTHS 12
#define YEARS 5

int main(void)
{
	const float rain[YEARS][MONTHS] = 
	{
		{4.3,4.3,3.2,2.2,5.2,1.1,8.4,9.1,5.9,1.1,6.4,1.3},
		{5.2,1.5,3.3,9.1,3.2,1.4,8.4,7.3,7.2,2.1,8.3,9.1},
		{5.5,8.4,9.2,5.3,9.2,2.5,9.3,1.4,4.3,3.7,5.3,3.4},
		{8.2,2.1,4.9,2.5,6.3,2.1,5.6,7.2,6.7,9.2,5.6,6.3},
		{2.6,7.5,2.2,7.4,2.2,1.5,3.6,9.3,2.4,6.3,7.2,9.5}
	};
	int year;
	int month;
	float subtot;
	float total;
	
	printf("ГОД	КОЛИЧЕСТВО ОСАДКОВ (в дюймах)\n");
	for(year = 0, total = 0; year < YEARS; year++)
	{
		for (month = 0,subtot = 0; month < MONTHS; month++)
		{
			subtot += rain[year][month];
		}
		printf("%5d %15.1f\n", 2010 + year, subtot);
		total += subtot;
	}
	printf("\nСреднегодовое количество осадков составляет %.1f дюймов\n\n", total  / YEARS);
	printf("СРЕДНЕМЕСЯЧНОЕ КОЛИЧЕСВТВО ОСАДКОВ : \n\n");
	printf("янв  фев  мар  арп  май  Июн  Июл  Авг  Сен  Окт  Ноя  Дек\n");
	for (month = 0; month < MONTHS; month++)
	{
		for (year = 0, subtot = 0; year < YEARS; year++)
		{
			subtot += rain[year][month];
		}
		printf("%4.1f", subtot / YEARS);
	}
	printf("\n");
	
	return 0;
}
