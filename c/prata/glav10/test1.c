#include <stdio.h>
#define SIZE 10
int main(void)
{
	int mass[SIZE] = {1,2,3,4,5,6,7,8};
	int *a = mass;
	int *b = &mass[1];
	
	printf("a = %p, *a = %d\n", a, *a);
	printf("b = %p, *b = %d\n", b, *b);
	*a++;
	printf("a = %p, *a = %d\n", a, *a);

	return 0;
}

