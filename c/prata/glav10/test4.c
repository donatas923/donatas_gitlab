#include <stdio.h>
#define SIZE 10
int summ(int *x, int *y);

int main(void)
{
	int mass[SIZE] = {1,2,3,4,5,6,7,8,9,10};
	int answer;
	answer = summ(mass, mass + SIZE);
	printf("Сумма элементов массива %d\n", answer);
	
	return 0;
}
int summ(int *x, int *y)
{
	int total;
	
	while (x < y)
	{
		total += *x;
		x++;
	}
	
	return total;
}
