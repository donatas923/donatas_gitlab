#include <stdio.h>
#define TARIF 10.0
#define PREV 1.5
#define TAX_PROCENT_300 15
#define TAX_PROCENT_150 20
#define TAX_PROCENT_OST 25

float tax(float);

int main (void)
{
	float hours = 1 ;	
	float total;
	float total_no_tax;
	float hours_overtime;
	float money_overtime;
	float total_no_tax_no_overtime;
	float tax_300;
	float tax_150;
	float ostatok;	
	float tax_ostatok;
	
	while (hours != 0)
	{
		printf("Ведите общее количесиво отработанных вами часов за неделю  : ");
		scanf("%f", &hours);
		if (hours == 0)
		{
			printf("Вы ничего не заработали\n");	
			continue;
		}
		if (hours > 168)
		{
			printf("Это не возможно. В неделе всего 168 часов.\n");
			continue;
		}
		if (hours < 40)
		{
		total_no_tax_no_overtime = hours * TARIF;
		printf("Общая сумма начислений без сверхурочных часов и вычета налогов равна = %.2f $\n", total_no_tax_no_overtime);
		tax(total_no_tax_no_overtime);
		}
		if (hours > 40)
		{
			hours_overtime = hours - 40;
			money_overtime = TARIF * 1.5 * hours_overtime;
			total_no_tax =  total_no_tax_no_overtime + money_overtime;
			printf("Сумма оплаты сверхурочных часов равна %.2f $ \n", money_overtime);
			printf("Общая сумма начислений с учетом сверхурочных часов, но без вычета налогов ровняется %.2f $ \n", total_no_tax);
			tax(total_no_tax);
		}
	
	}
	
	return 0;
}

float tax(float a)
{
	float tax_300;
	float tax_150;
	float total;
	float ostatok;
	float tax_ostatok;
	
	tax_300 = (300 * TAX_PROCENT_300) / 100;
	tax_150 = (150 * TAX_PROCENT_150) / 100;
	
	if (a < 300)
	{
		printf("Ваш заработок не облагается налогами\n");
	}
	else if (a == 300)
	{
		total = a - tax_300;
		printf("Общая сумма выплат со сверхурочными часами и вычетом налогов равна %.2f \n", total);
	}
	else if (a > 300 && a < 450)
	{
		ostatok = a - 300;
		tax_ostatok = (ostatok * TAX_PROCENT_OST) / 100;
		total = (a - tax_300) - tax_ostatok;
		printf("Общая сумма выплат со сверхурочными часами и вычетом налогов равна %.2f \n", total);
	}
	else if (a > 300 && a > 450)
	{
		ostatok =  a - (300 + 150);
		tax_ostatok = (ostatok * TAX_PROCENT_OST) / 100;
		total = a - (tax_300 + tax_150 + tax_ostatok);
		printf("Общая сумма выплат со сверхурочными часами и вычетами налогов равна %.2f \n", total);
	}
	
	return (total);	
}	
