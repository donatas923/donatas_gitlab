#include <stdio.h>

int main(void)
{
	long num;
	long sum;
	_Bool input_is_good;
	
	printf("Введите целое число для последующего суммирования (или q для  завершения программы ) : \n");
	input_is_good = (scanf("%ld", &num) == 1);
	while (input_is_good)
	{
		sum = sum + num;
		printf("Введите следующее число (или q для завершения программы) : \n");
		input_is_good = (scanf("%lf", &num) == 1 );
	}
	printf("сумма введенных целых чисел равна %ld\n", sum);
	
	return 0;
}

	

