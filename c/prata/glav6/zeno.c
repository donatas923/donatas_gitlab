#include <stdio.h>
int main(void)
{
	int t_ch;
	float time, power_of_2;
	int limit;
	
	printf("Введите желаемое количество элементов последовательности : ");
	scanf("%d", &limit);
	for (time = 0, power_of_2 = 1 , t_ch = 1; t_ch <= limit; t_ch++, power_of_2 *= 2.0)
	{
		time += 1.0 / power_of_2;
		printf("Время = %f, когда количество элементов = %d\n", time, t_ch);
	}
	
	return 0;
}

