#include <stdio.h>

float power(float n, int p);
int main(void)
{
	float x, xpow;
	int exp;
	
	printf("Введите число и положительную целую степнь, в которую число будет возведено.\n");
	printf("Для завершения программы введите q\n");
	while (scanf("%f %d", &x, &exp) == 2)
	{	
		xpow = power(x, exp);
		printf("%.3f в степени %d равно %.5g\n", x, exp, xpow);
		printf("Введите следующую пару чисел или q для завершения \n");
	}
		
	return 0;
}
float power(float n, int p)
{
	float pow = 1;
	int i;
	
	for (i = 1; i <= p; i++)
	{
		pow *= n;
	}
	
	return pow;
}

	
