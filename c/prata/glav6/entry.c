#include <stdio.h>

int main(void)
{
	const int secret_code = 23;
	int code_entered;
	
	printf("Чтобы войти в клуб лечения трискадекафобии,\n");
	printf("пожалйста, введите секретный код : ");
	scanf("%d", &code_entered);
	while (code_entered != secret_code)
	{
		printf("Чтобы войти в клуб лечения трискадекафобии,\n");
		printf("пожалуйста, введите секретный код : ");
		scanf("%d", &code_entered);
	}
	printf("Поздравляем! Вы вылечены!\n");
	
	return 0;
}
	
	
