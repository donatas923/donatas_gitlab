#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(void)
{
	printf("Некоторые пределы чисел для данной системы  \n ");
	printf("Наибольшее значение типа int : %d \n", INT_MAX);
	printf("Наибольшее значение типа long long : %lld \n", LLONG_MAX);
	
	return 0;
}
