// prizza.c -- использует константы, определенные применительно к пище

#include <stdio.h>
#define PI 3.14159

int main(void)
{
	float area, circum, radius;

	printf("Каков радиус вашей пиццы? \n ");
	scanf("%f", &radius);
	area = PI * radius * radius;
	circum = 2.0 * PI * radius;
	printf("Основные параметры вашей пиццы : \n");
	printf("Длинна окружности = %1.2f, площать = %1.2f \n", circum, area);
	
	return 0;
}

